import React, { Component } from 'react';
import { Widget, addResponseMessage, addLinkSnippet } from 'react-chat-widget';
import {ApiAiClient} from "api-ai-javascript/es6/ApiAiClient";

import './App.css';

import logo from './robot.png';
const client = new ApiAiClient({accessToken: '4d148ab4d50a4c499fd0bf445e7f5acf'});


class App extends Component {
  componentDidMount() {
    addResponseMessage("Привет, я XBot!");
  }

  handleNewUserMessage = (newMessage) => {
    if (newMessage.includes('отчет')) {
      addLinkSnippet({
        title: 'Отчет',
        link: 'https://bot.rosatom.ru/safjni.xls',
      });
      return
    }

    client
      .textRequest(newMessage)
      .then(response => addResponseMessage(response.result.fulfillment.speech))
  };

  render() {
    return (
      <div className="App">
        <Widget
          handleNewUserMessage={this.handleNewUserMessage}
          profileAvatar={logo}
          title="XBot"
          subtitle={null}
          senderPlaceHolder="Введите сообщение..."
        />
      </div>
    );
  }
}

export default App;
